//
//  NewsUseCaseProvider.swift
//  News18
//
//  Created by Sergey Blagodatskikh on 24/09/2019.
//  Copyright © 2019 Sergey Blagodatskikh. All rights reserved.
//

import Foundation
import Moya


// MARK: - UseCaseProvider Implementation

public final class NewsUseCaseProviderImpl: NewsUseCaseProvider {
  
  private let provider: MoyaProvider<NewsAPI>
  
  public init() {
    provider = MoyaProvider<NewsAPI>()
  }
  
  func makeNewsUseCase() -> NewsUseCase {
    return NewsUseCaseImpl(provider: provider)
  }
  
  func makeNewsDetailUseCase() -> NewsDetailUseCase {
    return NewsDetailUseCaseImpl(provider: provider)
  }
  
}
