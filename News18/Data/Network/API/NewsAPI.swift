//
//  NewsAPI.swift
//  news
//
//  Created by Vladimir Shutov on 15/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import Moya

enum NewsAPI {
  ///GET news18.ru/api/v4?datetime=2019-08-15%2012:54:18&q=listNews&skip=0&take=20
  case news(datetime: String, skip: Int)

  /// GET news18.ru/api/v4?id=00fcd370-bf32-11e9-808b-0108dd9a7e11&q=news
  case detailNews(_ id: String)
}

extension NewsAPI: TargetType {
  var baseURL: URL {
    return URL(string: Constants.Network.NewsApi.baseUrl.rawValue)!
  }

  var path: String {
    return Constants.Network.NewsApi.path.rawValue
  }

  var method: Method {
    return .get
  }

  var sampleData: Data {
    return Data()
  }

  var task: Task {

    switch self {

    case .news(let datetime, let skip):
      return .requestParameters(parameters: ["datetime": datetime,
                                             "q": "listNews",
                                             "skip": skip,
                                             "take": 20],
                                encoding: URLEncoding.queryString)

    case .detailNews(let id):
      return .requestParameters(parameters: ["id": id,
                                             "q": "news"],
                                encoding: URLEncoding.queryString)
    }

  }

  var headers: [String: String]? {
    return nil
  }

}
