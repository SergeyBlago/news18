//
//  NewsService.swift
//  news
//
//  Created by Vladimir Shutov on 15/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import Moya
import RxSwift

// MARK: - NewsUseCase Implementation

final class NewsUseCaseImpl: NewsUseCase {
  
  private let disposeBag = DisposeBag()
  private let provider: MoyaProvider<NewsAPI>

// MARK: - Inits
  
  init(provider: MoyaProvider<NewsAPI>) {
    self.provider = provider
  }
  
  func obtainNews(skipNews: Int)  -> Single<[NewsPreviewStored]> {
    var date = NSDate().description
    date.removeLast(6)
    
    let sequence = provider.rx.request(.news(datetime: date, skip: skipNews))
      .filterSuccessfulStatusAndRedirectCodes()
      .map([NewsPreviewStored].self)
      .catchError { [weak self] error in
        guard let error = error as? MoyaError,
          let self = self else {
            throw Errors.SystemError.unknown
        }
        throw self.errorHandler(error)
      }
      .retry(2)
    
    return sequence
  }
  
// MARK: - Helpers
  
  private func errorHandler(_ error: MoyaError) -> Errors.Network {

    switch error {

    case MoyaError.underlying(let ulError, _):

      switch (ulError as NSError).code {

      case NSURLErrorNotConnectedToInternet,
           NSURLErrorNetworkConnectionLost:
        return Errors.Network.noInternet(error)

      case NSURLErrorTimedOut,
           NSURLErrorCannotFindHost,
           NSURLErrorCannotConnectToHost:
        return Errors.Network.serverUnavailable(error)

      default:
        return Errors.Network.other(error)
      }

    case MoyaError.encodableMapping,
         MoyaError.objectMapping,
         MoyaError.jsonMapping,
         MoyaError.stringMapping:
      
      return Errors.Network.badResponse(error)

    default:
      return Errors.Network.other(error)
    }
  }
}
