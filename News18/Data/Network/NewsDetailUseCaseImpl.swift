//
//  NewsDetailUseCase.swift
//  News18
//
//  Created by Sergey Blagodatskikh on 26/09/2019.
//  Copyright © 2019 Sergey Blagodatskikh. All rights reserved.
//

import Foundation
import RxSwift
import Moya

final class NewsDetailUseCaseImpl: NewsDetailUseCase {
  
  private let disposeBag = DisposeBag()
  private let provider: MoyaProvider<NewsAPI>

// MARK: - Inits
  
  init(provider: MoyaProvider<NewsAPI>) {
    self.provider = provider
  }
  
  func obtainNewsById(_ id: String) -> Single<NewsDetailStored> {
    let sequence = provider.rx.request(.detailNews(id))
      .filterSuccessfulStatusAndRedirectCodes()
      .map(NewsDetailStored.self)
      .catchError { [weak self] error in
        guard let error = error as? MoyaError,
          let self = self else {
            throw Errors.SystemError.unknown
        }
        throw self.errorHandler(error)
      }
      .retry(2)
    
    return sequence
  }
  
  private func errorHandler(_ error: MoyaError) -> Errors.Network {

    switch error {

    case MoyaError.underlying(let ulError, _):

      switch (ulError as NSError).code {

      case NSURLErrorNotConnectedToInternet,
           NSURLErrorNetworkConnectionLost:
        return Errors.Network.noInternet(error)

      case NSURLErrorTimedOut,
           NSURLErrorCannotFindHost,
           NSURLErrorCannotConnectToHost:
        return Errors.Network.serverUnavailable(error)

      default:
        return Errors.Network.other(error)
      }

    case MoyaError.encodableMapping,
         MoyaError.objectMapping,
         MoyaError.jsonMapping,
         MoyaError.stringMapping:
      
      return Errors.Network.badResponse(error)

    default:
      return Errors.Network.other(error)
    }
  }
}
