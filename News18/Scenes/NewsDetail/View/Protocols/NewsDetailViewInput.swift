//
//  NewsDetailViewInput.swift
//  news
//
//  Created by Sergey Blagodatskikh on 19/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import Foundation

protocol NewsDetailViewInput: class {
  func updateUI(plain: NewsDetailPlain)
}
