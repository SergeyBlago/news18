//
//  NewsDetailController.swift
//  news
//
//  Created by Sergey Blagodatskikh on 19/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//
import Kingfisher
import UIKit
import RxCocoa
import RxSwift

final class NewsDetailController: UIViewController, DIConfigurable {
  
  // MARK: - IBOutlet

  @IBOutlet weak var titleArticle: UILabel!
  @IBOutlet weak var imageArticle: UIImageView!
  @IBOutlet weak var article: UITextView!

  // MARK: - Private properties
  
  private let viewModel: NewsDetailViewModel
  private let disposeBag: DisposeBag
  
  // MARK: - Inits

  init(container: Container) {
    viewModel = container.viewModel
    disposeBag = DisposeBag()
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Lifecycle

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    //imageArticle.isHidden = true
    bindViewModel()
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    imageArticle.kf.cancelDownloadTask()
  }
  
  // MARK: - Bind viewModel
  
  func bindViewModel() {
    let output = viewModel.transform(input: NewsDetailViewModel.Input())
    
    output.newsDetail
      .subscribe(onSuccess: { [weak self] plain in
      
        guard let self = self else {
          return
        }
        self.titleArticle.text = plain.title
        self.article.text = plain.article
        
        if let imageUrl = plain.imageURL {
          self.imageArticle.kf.setImage(with: imageUrl)
          self.imageArticle.isHidden = false
        } else {
          self.imageArticle.isHidden = true
        }
      })
      .disposed(by: disposeBag)
  }

}
// MARK: - Stub Entitys

extension NewsDetailController {
  struct Container {
    let viewModel: NewsDetailViewModel
    //let output: NewsDetailViewOutput
  }
}
