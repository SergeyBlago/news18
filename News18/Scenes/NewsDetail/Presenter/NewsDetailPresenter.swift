//
//  NewsDetailPresenter.swift
//  news
//
//  Created by Sergey Blagodatskikh on 19/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import Foundation
import RxSwift

final class NewsDetailPresenter: DIConfigurable {

  // MARK: - Private properties

  private let router: NewsDetailRouter
  private let newsService: NewsDetailUseCase
  private let disposeBag: DisposeBag

  private weak var view: NewsDetailViewInput?
  private var newsId: String
  

  // MARK: - Inits

  init(container: Container) {
    disposeBag = DisposeBag()
    newsService = container.newsService
    router = container.router
    newsId = container.newsId
  }

  func inject(view: NewsDetailViewInput) {
    self.view = view
  }

}

// MARK: - NewsDetailViewOutput

extension NewsDetailPresenter: NewsDetailViewOutput {

  func viewDidAppear() {
    obtainNewsByIdRx(newsId)
  }

}

// MARK: - NewsDetailViewOutput Functions

extension NewsDetailPresenter {
  
  private func obtainNewsByIdRx(_ id: String) {
    newsService.obtainNewsById(id)
      .map { stored -> NewsDetailPlain in
        return NewsDetailPlain(stored: stored)
      }
      .subscribe(onSuccess: { [weak self] plain in
        self?.view?.updateUI(plain: plain)
      }, onError: { [weak self] error in
        guard let error = error as? Error else {
            self?.router.presentError(Errors.SystemError.unknown)
            return
        }
        self?.router.presentError(error)
      })
      .disposed(by: disposeBag)
    
  }
  
}


// MARK: - Stub Entitys

extension NewsDetailPresenter {

  struct Container {
    let newsService: NewsDetailUseCase
    let router: NewsDetailRouter
    let newsId: String
  }

}
