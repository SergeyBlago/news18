//
//  NewsDetailViewModel.swift
//  News18
//
//  Created by Sergey Blagodatskikh on 26/09/2019.
//  Copyright © 2019 Sergey Blagodatskikh. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

final class NewsDetailViewModel: DIConfigurable {
  
  //MARK: - Private properties
  
  let disposeBag: DisposeBag
  let useCase: NewsDetailUseCase
  let router: NewsDetailRouter
  let newsId: String
  
  
  //MARK: - Inits
  
  init(container: Container) {
    disposeBag = DisposeBag()
    useCase = container.useCase
    router = container.router
    newsId = container.newsId
  }
  
}
  //MARK: - Stub entites

  extension NewsDetailViewModel {
  
  struct Container {
    let router: NewsDetailRouter
    let useCase: NewsDetailUseCase
    let newsId: String
  }
}

extension NewsDetailViewModel: ViewModelType {
  struct Input {
    
  }
  
  struct Output {
    let newsDetail: Single<NewsDetailPlain>
  }
  
  func transform(input: Input) -> Output {
    
    let newsDetail = useCase.obtainNewsById(newsId)
      .map {
        NewsDetailPlain(stored: $0)
      }
      .do(onError: { [weak self] (error) in
        self?.router.presentError(error.localizedDescription)
      })
    
    return Output(newsDetail: newsDetail)
  }
  
}

