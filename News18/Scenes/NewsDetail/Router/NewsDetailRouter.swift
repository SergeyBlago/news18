//
//  NewsDetailRouter.swift
//  news
//
//  Created by Sergey Blagodatskikh on 19/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import Foundation

final class NewsDetailRouter: Router<NewsDetailController>, NewsRouter.Routes {

  typealias Routes = ErrorRoute
}
