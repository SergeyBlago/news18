//
//  NewsDetailModule.swift
//  news
//
//  Created by Sergey Blagodatskikh on 19/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import UIKit
struct NewsDetailModule {

  let view: UIViewController

  init(transition: Transition?, id: String) {

    let router = NewsDetailRouter()
    
    let newsDetailUseCase = NewsUseCaseProviderImpl().makeNewsDetailUseCase()
    let viewModelContainer = NewsDetailViewModel.Container(router: router, useCase: newsDetailUseCase, newsId: id)

    let viewModel = NewsDetailViewModel(container: viewModelContainer)
    

    let viewContainer = NewsDetailController.Container(viewModel: viewModel)
    let view = NewsDetailController(container: viewContainer)

    router.viewController = view
    router.openTransition = transition

    self.view = view
  }

}
