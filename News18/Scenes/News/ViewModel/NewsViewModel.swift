//
//  NewsViewModel.swift
//  News18
//
//  Created by Sergey Blagodatskikh on 18/09/2019.
//  Copyright © 2019 Sergey Blagodatskikh. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class NewsViewModel: DIConfigurable {
  
  private let router: NewsRouter
  private let useCase: NewsUseCase
  private var newsStored: [NewsPreviewStored] = []

  init(container: Container) {
    router = container.router
    useCase = container.useCase
  }
  
}

extension NewsViewModel {
  
  struct Container {
    let router: NewsRouter
    let useCase: NewsUseCase
  }
  
}

extension NewsViewModel: ViewModelType {
  
  struct Input {
    let viewWillAppearTrigger: Observable<Void>
    let newsSelectedTrigger: Observable<IndexPath>
    let nextPageTrigger: Observable<Void>
  }
  
  struct Output {
    let news: Driver<[NewsPreviewPlain]>
    let toSelectedNews: Driver<String>
  }
  
  func transform(input: Input) -> Output {
  
    let nextPageTrigger = input.nextPageTrigger
    let viewWillAppearTrigger = input.viewWillAppearTrigger
    
    let news = Observable
      .merge(viewWillAppearTrigger, nextPageTrigger)
      .flatMapLatest { [weak self] _ -> Single<[NewsPreviewStored]> in
        guard let self = self else {
          throw Errors.SystemError.unknown
        }
        return self.useCase.obtainNews(skipNews: self.newsStored.count)
      }
      .map { [weak self] stored in
        guard let self = self else {
          throw Errors.SystemError.unknown
        }
        return self.newsStored + stored
      }
      .distinctUntilChanged()
      .do(onNext: { [weak self] news in
        self?.newsStored = news
        }, onError: { [weak self] error in
          self?.router.presentError(error.localizedDescription)
      })
      .map { stored -> [NewsPreviewPlain] in
        return stored.map {
          NewsPreviewPlain(stored: $0)
        }
      }
      .share()
    
   
    let toSelectedNews = input.newsSelectedTrigger
        .withLatestFrom(news) { indexPath, items -> String in
          let index = indexPath.row
          
          guard items.endIndex >= index else {
            throw Errors.SystemError.unknown
          }
          return items[index].id
        }
      .do(onNext: { [weak self] id in
        self?.router.showDetailNews(byId: id)
        }, onError: { [weak self] error in
          self?.router.presentError(error.localizedDescription)
      })
      
    
    return Output(news: news.asDriver(onErrorJustReturn: []),
                  toSelectedNews: toSelectedNews.asDriver(onErrorDriveWith: Driver.empty()))
  }
}


