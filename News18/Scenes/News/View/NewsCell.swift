//
//  NewsCell.swift
//  news
//
//  Created by Sergey Blagodatskikh on 16/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import UIKit
import Kingfisher

class NewsCell: UITableViewCell {
  
// MARK: - Private properties
  
  @IBOutlet private weak var label: UILabel!
  @IBOutlet private weak var Icon: UIImageView!

  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    Icon.kf.cancelDownloadTask()
  }

// MARK: - Configure
  func configure(_ news: NewsPreviewPlain) {
    label.text = news.title
    
    if let url = news.imageURL {
      Icon.isHidden = false
      Icon.kf.indicatorType = .activity
      Icon.kf.setImage(with: url)
    } else {
      Icon.isHidden = true
    }
    
  }

}
