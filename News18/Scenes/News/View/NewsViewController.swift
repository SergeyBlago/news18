//
//  ViewController.swift
//  news
//
//  Created by Vladimir Shutov on 15/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//
import UIKit
import RxSwift
import RxCocoa

final class NewsViewController: UIViewController, DIConfigurable{
  
  // MARK: - IBOutlet

  @IBOutlet weak var table: UITableView!

  // MARK: - Private properties

  private let viewModel: NewsViewModel
  private let disposeBag = DisposeBag()
  
  // MARK: - Inits

  init(container: Container) {
    viewModel = container.viewModel
    
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // MARK: - Lifecycle

  override func viewDidLoad() {
    super.viewDidLoad()
    let nib = UINib(nibName: Constants.Scenes.News.cellIdentifier.rawValue, bundle: nil)
    table?.register(nib, forCellReuseIdentifier: Constants.Scenes.News.cellIdentifier.rawValue)
    configureTableView()
    bindViewModel()
    
  }
  // MARK: - Configurate
  private func configureTableView() {
    table.rowHeight = UITableView.automaticDimension
    table.estimatedRowHeight = 400
  }
  
  // MARK: - Bind ViewModel
  
  func bindViewModel() {
    
    let viewWillAppear = rx
      .sentMessage(#selector(UIViewController.viewDidAppear(_:)))
      .map {_ in }
  
    
    
    let nextPageTrigger = table.rx
      .reachedBottom(offset: Constants.contentOffset)
      .asObservable()
    
    
    let selectedItem = table.rx.itemSelected.asObservable()
    
    
    let input = NewsViewModel.Input(viewWillAppearTrigger: viewWillAppear, newsSelectedTrigger: selectedItem, nextPageTrigger: nextPageTrigger)
    let output = viewModel.transform(input: input)
    
    output.news
          .drive(table.rx.items(cellIdentifier: Constants.Scenes.News.cellIdentifier.rawValue, cellType: NewsCell.self)) { index, plain, cell in
            cell.configure(plain)
          }
          .disposed(by: disposeBag)
    
    output.toSelectedNews
      .drive()
      .disposed(by: disposeBag)
  }
}
// MARK: - Stub Entitys

extension NewsViewController {
  
  struct Container {
    let viewModel: NewsViewModel
  }
  
}


