//
//  NewsModule.swift
//  news
//
//  Created by Vladimir Shutov on 15/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import UIKit

struct NewsModule {

  let view: UIViewController

  init(transition: Transition? ) {
    let router = NewsRouter()

    let newsUseCase = NewsUseCaseProviderImpl().makeNewsUseCase()

    let viewModelContainer = NewsViewModel.Container(router: router, useCase: newsUseCase)
    let viewModel = NewsViewModel(container: viewModelContainer )
    
    let viewContainer = NewsViewController.Container(viewModel: viewModel)
    let view = NewsViewController(container: viewContainer)

    router.viewController = view
    router.openTransition = transition
    
    self.view = view
  }
}
