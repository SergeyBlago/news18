//
//  DIConfigurable.swift
//  news
//
//  Created by Vladimir Shutov on 15/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import Foundation

protocol DIConfigurable {
  associatedtype Container
  init(container: Container)
}
