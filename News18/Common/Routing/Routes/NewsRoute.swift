//
//  MainRoute.swift
//  news
//
//  Created by Vladimir Shutov on 15/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import UIKit

protocol NewsRoute {
  func showNews()
}

extension NewsRoute where Self: RouterProtocol {

  func showNews() {
    let transition = WindowNavigationTransition()
    let module = NewsModule(transition: transition)
    openTransition(module.view, transition: transition)
  }
}
