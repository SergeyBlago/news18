//
//  NewsDetailRoute.swift
//  news
//
//  Created by Sergey Blagodatskikh on 20/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import Foundation

protocol NewsDetailRoute {
  func showDetailNews(byId id: String)
}

extension NewsDetailRoute where Self: RouterProtocol {

  func showDetailNews(byId id: String) {

    let push = PushTransition(animator: nil, isAnimated: true)
    let module = NewsDetailModule(transition: push, id: id)

    openTransition(module.view, transition: push)
  }

}
