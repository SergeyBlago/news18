//
//  ErrorRoute.swift
//  news
//
//  Created by Vladimir Shutov on 15/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import UIKit

protocol ErrorRoute {
  func presentError(_ error: Error)
  func presentError(_ message: String?)
}

extension ErrorRoute where Self: RouterProtocol {
  func presentError(_ error: Error) {
    let alertVC = UIAlertController(
      title: error.failureReason,
      message: error.errorDescription,
      preferredStyle: .alert
    )
    let acceptAction = UIAlertAction(title: "OK".l10n(), style: .default, handler: nil)
    alertVC.addAction(acceptAction)
    viewController?.present(alertVC, animated: true, completion: nil)
  }

  func presentError(_ message: String?) {
    let alertVC = UIAlertController(
      title: "ERROR".l10n(),
      message: message,
      preferredStyle: .alert
    )
    let acceptAction = UIAlertAction(title: "OK".l10n(), style: .default, handler: nil)
    alertVC.addAction(acceptAction)
    viewController?.present(alertVC, animated: true, completion: nil)
  }
}
