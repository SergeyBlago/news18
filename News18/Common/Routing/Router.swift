//
//  Router.swift
//  news
//
//  Created by Vladimir Shutov on 15/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import UIKit

protocol Closable: class {
  func closeOpenTransition()
}

protocol RouterProtocol: class {
  associatedtype GenericController: UIViewController
  var viewController: GenericController? { get }

  func openTransition(_ viewController: UIViewController, transition: Transition)
}

class Router<GenericController: UIViewController>: RouterProtocol, Closable {

  weak var viewController: GenericController?
  var openTransition: Transition?

  func openTransition(_ viewController: UIViewController, transition: Transition) {
    transition.viewController = self.viewController
    transition.open(viewController)
  }

  func closeOpenTransition() {
    guard let openTransition = openTransition else {
      assertionFailure("You should specify an open transition in order to close a module.")
      return
    }
    guard let viewController = viewController else {
      assertionFailure("Nothing to close.")
      return
    }
    openTransition.close(viewController)
  }
}
