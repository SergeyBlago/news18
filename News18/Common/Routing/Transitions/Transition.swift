//
//  Transition.swift
//  news
//
//  Created by Vladimir Shutov on 15/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import UIKit

protocol Transition: class {
  var viewController: UIViewController? { get set }

  func open(_ viewController: UIViewController)
  func close(_ viewController: UIViewController)
}
