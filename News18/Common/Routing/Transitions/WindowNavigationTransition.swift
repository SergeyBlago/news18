//
//  WindowNavigationTransition.swift
//  news
//
//  Created by Vladimir Shutov on 15/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import UIKit

class WindowNavigationTransition: NSObject {
  weak var viewController: UIViewController?
}

// MARK: - Transition

extension WindowNavigationTransition: Transition {

  func open(_ viewController: UIViewController) {
    guard
      let appDelegate = UIApplication.shared.delegate as? AppDelegate,
      let window = appDelegate.window else {
        return
    }
    let navigationController = UINavigationController(rootViewController: viewController)
    window.rootViewController = navigationController
    UIView.transition(with: window,
                      duration: Constants.animationDuration,
                      options: .transitionCrossDissolve,
                      animations: nil,
                      completion: nil)
    window.makeKeyAndVisible()
  }

  func close(_ viewController: UIViewController) { }

}
