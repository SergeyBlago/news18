//
//  Constants.swift
//  news
//
//  Created by Vladimir Shutov on 15/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import Foundation
import UIKit

enum Constants {

  static var animationDuration: Double {
    return 0.25
  }
  static var contentOffset: CGFloat {
    return 100
  }

  enum Scenes {
    
    enum News: String {
      case cellIdentifier = "NewsCell"
    }

    enum NewsDetail {

    }

  }

  enum Network {
    enum NewsApi: String {
      case baseUrl = "http://news18.ru"
      case path = "api/v4"

    }

    enum NewsDetailApi: String {
      case baseUrl = "http://news18.ru"
      case path = "api/v4"

    }

  }

}
