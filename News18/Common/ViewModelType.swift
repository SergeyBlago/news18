//
//  ViewModelType.swift
//  News18
//
//  Created by Sergey Blagodatskikh on 19/09/2019.
//  Copyright © 2019 Sergey Blagodatskikh. All rights reserved.
//

import Foundation

protocol ViewModelType {
  associatedtype Input
  associatedtype Output
  
  func transform(input: Input) -> Output
}

