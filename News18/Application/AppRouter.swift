//
//  AppRouter.swift
//  news
//
//  Created by Vladimir Shutov on 15/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import UIKit

final class AppRouter: Router<UIViewController>, AppRouter.Routes {
  typealias Routes = NewsRoute & ErrorRoute

}
