//
//  NewsDetailUseCase.swift
//  News18
//
//  Created by Sergey Blagodatskikh on 26/09/2019.
//  Copyright © 2019 Sergey Blagodatskikh. All rights reserved.
//

import Foundation
import RxSwift

protocol NewsDetailUseCase: class {
  func obtainNewsById(_ id: String) -> Single<NewsDetailStored>
}
