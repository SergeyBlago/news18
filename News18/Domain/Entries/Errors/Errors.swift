//
//  Error.swift
//  news
//
//  Created by Vladimir Shutov on 15/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import Foundation
import L10n_swift
import Moya

protocol Error: LocalizedError {}

enum Errors {
  /// Интернет ошибки
  enum Network: Error {
    case noInternet(_ error: MoyaError)
    case serverUnavailable(_ error: MoyaError)
    case badResponse(_ error: MoyaError)
    case badURL(_ error: MoyaError)
    case badStatusCode(_ code: Int)
    case other(_ error: MoyaError)
  }

  /// Decoding
  enum Decodable: Error {
    case badDecoding(_ error: DecodingError )
  }

  /// Прочие ошибки
  enum SystemError: Error {
    /// Swift ошибка
    case error(Swift.Error)
    /// NSError ошибка
    case nsError(NSError)
    /// Неизвестная ошибка
    case unknown
  }
}

// MARK: - SystemError

extension Errors.SystemError {
  
  var failureReason: String? {
    switch self {
    case .error:
      return nil
    case .nsError(let error):
      return error.localizedFailureReason
    case .unknown:
      return "ERROR.SYSTEM_ERROR.REASON".l10n()
    }
  }
  
  var errorDescription: String? {
    switch self {
    case .error(let error):
      return error.localizedDescription
    case .nsError(let error):
      return error.localizedDescription
    case .unknown:
      return "ERROR.SYSTEM_ERROR.DESCRIPTION".l10n()
    }
  }
}

// MARK: - SystemError

extension Errors.Network {
  var failureReason: String? {
    switch self {
      
    case .noInternet(let error),
         .serverUnavailable(let error),
         .badResponse(let error),
         .badURL(let error),
         .other(let error):
      return error.failureReason
      
    case .badStatusCode:
      return nil
    }
  }
  
  var errorDescription: String? {
    switch self {
      
    case .noInternet(let error),
         .serverUnavailable(let error),
         .badResponse(let error),
         .badURL(let error),
         .other(let error):
      return error.errorDescription
      
    case .badStatusCode(let code):
      return "bad status code \(code)"
      
    }
  }
}

// MARK: - SystemError
extension Errors.Decodable {
  var failureReason: String? {
    switch self {
    case .badDecoding(let error):
      return error.failureReason
    }
  }
  
  var errorDescription: String? {
    switch self {
    case .badDecoding(let error):
      return error.errorDescription
    }
  }
}
