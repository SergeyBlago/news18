//
//  NewsDetail.swift
//  news
//
//  Created by Sergey Blagodatskikh on 15/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import Foundation

struct NewsDetailStored: Codable {

  let id: String
  let title: String
  let content: String
  let iconURL: String?
  let source: String
  let sourceURL: String
  let sourceIconURL: String?
  let link: String
  let datetime: String
  let related: String

  enum CodingKeys: String, CodingKey {
    case id, title, content, source
    case iconURL = "iconUrls"
    case sourceURL = "sourceUrl"
    case sourceIconURL = "sourceIconUrl"
    case link, datetime, related
  }
}
