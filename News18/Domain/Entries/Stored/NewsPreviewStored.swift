//
//  NewsPreview.swift
//  news
//
//  Created by Sergey Blagodatskikh on 15/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import Foundation

struct NewsPreviewStored: Codable, Equatable{
  let id: String
  let title: String
  let source: String
  let sourceURL: String
  let datetime: String
  let iconURL: String?

  enum CodingKeys: String, CodingKey {
    case id, title, source, datetime
    case sourceURL = "sourceUrl"
    case iconURL = "iconUrls"
  }
}
