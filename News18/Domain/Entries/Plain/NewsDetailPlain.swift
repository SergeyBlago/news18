//
//  NewsDetailPlain.swift
//  news
//
//  Created by Sergey Blagodatskikh on 19/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import Foundation
import Kanna

struct NewsDetailPlain {
  let title: String
  let article: String
  var imageURL: URL?
  var sourceImageURL: URL?

  init(stored: NewsDetailStored) {
    title = stored.title

    let html = stored.content
    let doc = try? Kanna.HTML(html: html, encoding: String.Encoding.utf8)
    
    if let text = doc?.text {
       article = text
    } else {
      article = ""
    }
    
   
    if stored.iconURL != "[]",
      var iconURLString = stored.iconURL {

      let startIndex = iconURLString.startIndex
      let endIndex = iconURLString.endIndex
      let startEndIndex = iconURLString.index(startIndex, offsetBy: 2)
      let endStartIndex = iconURLString.index(endIndex, offsetBy: -2)

      iconURLString.removeSubrange(endStartIndex..<endIndex)
      iconURLString.removeSubrange(startIndex..<startEndIndex)

      iconURLString = iconURLString.replacingOccurrences(of: "\\", with: "")

      if let url = URL(string: iconURLString) {
        self.imageURL = url
      }

    }
  }

}
