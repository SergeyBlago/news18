//
//  News.swift
//  news
//
//  Created by Vladimir Shutov on 15/08/2019.
//  Copyright © 2019 Vladimir Shutov. All rights reserved.
//

import Foundation

struct NewsPreviewPlain {

  let id: String
  let title: String
  let source: String
  let datetime: String
  var imageURL: URL?

  init(stored: NewsPreviewStored) {
    self.id = stored.id
    self.title = stored.title
    self.datetime = stored.datetime
    self.source = stored.source

    if stored.iconURL != "[]",
      var iconURLString = stored.iconURL {

      let startIndex = iconURLString.startIndex
      let endIndex = iconURLString.endIndex
      let startEndIndex = iconURLString.index(startIndex, offsetBy: 2)
      let endStartIndex = iconURLString.index(endIndex, offsetBy: -2)

      iconURLString.removeSubrange(endStartIndex..<endIndex)
      iconURLString.removeSubrange(startIndex..<startEndIndex)

      iconURLString = iconURLString.replacingOccurrences(of: "\\", with: "")

      if let url = URL(string: iconURLString) {

        self.imageURL = url

      }

    }
  }

}
